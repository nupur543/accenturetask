//
//  HomeViewController.swift
//  Accenture_Task
//
//  Created by Nupur Sharma on 18/02/21.
//  Copyright © 2021 Nupur Sharma. All rights reserved.
//

import UIKit
import ReactiveKit
import CoreData

class HomeViewController: UIViewController {

    @IBOutlet weak var lblTemp: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    private var weatherViewModel : WeatherHomeViewModel!
    private var arrInitialPositions = [CGPoint]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        weatherViewModel = WeatherHomeViewModel()
        indicator.startAnimating()
        bindViewModel()
        arrInitialPositions.append(lblCity.center)
        arrInitialPositions.append(lblTemp.center)
        arrInitialPositions.append(lblDesc.center)
       // view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
    }
    
    @objc func handleTap() {

        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
            self.lblCity.transform = CGAffineTransform(translationX: -30, y: 0)
            
        }, completion: { (_) in
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.lblCity.transform = self.lblCity.transform.translatedBy(x: 0, y: -650)
                self.lblCity.alpha = 0
        
            }, completion: { (_) in
                
            })
            
        })
        
        UIView.animate(withDuration: 0.5, delay: 0.2, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                  self.lblTemp.transform = CGAffineTransform(translationX: -20, y: 0)
                  
              }, completion: { (_) in
                  
                  UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                      self.lblTemp.transform = self.lblTemp.transform.translatedBy(x: 0, y: -650)
                      self.lblTemp.alpha = 0
                  }, completion: { (_) in
                      
                  })
                  
              })
        
        UIView.animate(withDuration: 0.5, delay: 0.5, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                  self.lblDesc.transform = CGAffineTransform(translationX: -10, y: 0)
                  
              }, completion: { (_) in
                  
                  UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
                      self.lblDesc.transform = self.lblDesc.transform.translatedBy(x: 0, y: -650)
                      self.lblDesc.alpha = 0
                      
                  }, completion: { (_) in
                    self.lblCity.alpha = 1
                    self.lblCity.transform = CGAffineTransform.identity
                    self.lblTemp.alpha = 1
                    self.lblTemp.transform = CGAffineTransform.identity
                    self.lblDesc.alpha = 1
                    self.lblDesc.transform = CGAffineTransform.identity
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        self.performSegue(withIdentifier: "showDetailSegue", sender: self)
                    }
                  })
                  
              })
    }

    fileprivate func bindViewModel() {
    
        weatherViewModel?.strCity.bind(to: self.reactive.bond(setter: { [weak self] (view, city) in
            self?.indicator.stopAnimating()
            self?.lblCity.text = city
        }))
        
        weatherViewModel?.strTemp.bind(to: self.reactive.bond(setter: { [weak self] (view, temp) in
            self?.lblTemp.text = temp
        }))
        
        weatherViewModel?.strDesc.bind(to: self.reactive.bond(setter: { [weak self] (view, desc) in
            self?.lblDesc.text = desc
        }))
    }
    
    @IBAction func saveData(_ sender: UIBarButtonItem) {
        guard lblCity.text!.count > 0, lblTemp.text!.count > 0, lblDesc.text!.count > 0 else {
            print("get data first")
            return
            
        }
        saveWeatherDB()
    }
    
    @IBAction func showSavedData(_ sender: Any) {
        
        self.performSegue(withIdentifier: "showSavedSegue", sender: self)
    }
    @IBAction func goToDetails(_ sender: UIButton) {
        handleTap()
//
        
    }
    
    private func getTodayString() -> String{
        
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
        
        let year = components.year
        let month = components.month
        let day = components.day
        let hour = components.hour
        let minute = components.minute
         
        let today_string = String(hour!)  + ":" + String(minute!) +  " " + String(year!) + "-" + String(month!) + "-" + String(day!) + " "
        
        return today_string
        
    }
    
    private func saveWeatherDB() {
        guard let appDel = UIApplication.shared.delegate as? AppDelegate else { return  }
        
        let viewContext = appDel.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Weather", in: viewContext)!
        if let detail = NSManagedObject(entity: entity, insertInto: viewContext) as? Weather {
            detail.city = lblCity.text
            detail.desc = lblDesc.text
            detail.temp = lblTemp.text
            detail.date = getTodayString()
            
            do {
                try viewContext.save()
                let alert = UIAlertController(title: "Saved!", message: "Weather info saved!!", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } catch let error as NSError {
                print("Could not save ")
            }
        }
        
      

    
        
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier  == "showDetailSegue" {
            let dest = segue.destination as! DetailInfoController
            let details = weatherViewModel.getDetails()
            dest.detailViewModel = DetailInfoViewModel(humid: details.humid, speed: details.speed, visiblty: details.visiblty)
        }
    }
    
}
