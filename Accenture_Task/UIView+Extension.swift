//
//  UIView+Extension.swift
//  Accenture_Task
//
//  Created by Nupur Sharma on 20/02/21.
//  Copyright © 2021 Nupur Sharma. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func roudedCorner() {
        self.layer.cornerRadius = self.frame.height/2
        self.layer.masksToBounds = true
    }
}
