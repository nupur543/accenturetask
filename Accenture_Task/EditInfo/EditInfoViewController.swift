//
//  EditInfoViewController.swift
//  Accenture_Task
//
//  Created by Nupur Sharma on 20/02/21.
//  Copyright © 2021 Nupur Sharma. All rights reserved.
//

import UIKit
import ReactiveKit
import CoreData

class EditInfoViewController: UIViewController {

    @IBOutlet weak var txtTemp: UITextField!
    @IBOutlet weak var txtDesc: UITextField!
    
    var editInfoViewModel: EditInfoViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtTemp.text! = editInfoViewModel?.temp ?? ""
        txtDesc.text! = editInfoViewModel?.desc ?? ""
    }
    
    @IBAction func savepressed(_ sender: Any) {
        
        if ifValidInfo() {
            editInfoViewModel?.updateInfo(txtTemp: txtTemp.text!, txtDesc: txtDesc.text!)
            navigationController?.popViewController(animated: true)
        }
    }
    
    private func ifValidInfo() -> Bool{
    
        guard txtTemp.text!.count > 0 , txtDesc.text!.count > 0 else {
            let alert = UIAlertController(title: "Oops", message: "Enter something to save.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EditInfoViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtTemp {
            let parsed = textField.text!.replacingOccurrences(of: " °F", with: "")
            textField.text! = parsed
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtTemp {
          let text = textField.text!
            if !(text.contains("°F")) {
                textField.text = text + " °F"
            }
        }
    }
}
