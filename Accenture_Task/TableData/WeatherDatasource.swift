//
//  WeatherDatasource.swift
//  Accenture_Task
//
//  Created by Nupur Sharma on 18/02/21.
//  Copyright © 2021 Nupur Sharma. All rights reserved.
//

import Foundation
import UIKit

class WeatherDatasource : NSObject, UITableViewDataSource {
    
    var formData: [WeatherModel] = []
    var identifier = String()
    var deleteData : ((_ row: Int) -> ()) = {_ in }
       
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return formData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "weatherCellID", for: indexPath) as! WeatherTableViewCell
        let item = formData[indexPath.row]
        cell.lblTemp.text = "Temperature: \(item.temp)"
        cell.lblDate.text = "Date: \(item.date)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
           return true
       }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
         if editingStyle == .delete {
            deleteData(indexPath.row)
        }
    }
}
