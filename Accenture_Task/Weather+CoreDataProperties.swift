//
//  Weather+CoreDataProperties.swift
//  
//
//  Created by Nupur Sharma on 19/02/21.
//
//

import Foundation
import CoreData


extension Weather {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Weather> {
        return NSFetchRequest<Weather>(entityName: "Weather")
    }

    @NSManaged public var city: String?
    @NSManaged public var temp: String?
    @NSManaged public var desc: String?
    @NSManaged public var date: String?

}
