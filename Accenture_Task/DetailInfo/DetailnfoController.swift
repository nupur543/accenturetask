//
//  DetailInfoController.swift
//  Accenture_Task
//
//  Created by Nupur Sharma on 19/02/21.
//  Copyright © 2021 Nupur Sharma. All rights reserved.
//

import UIKit
import ReactiveKit
import CoreData

class DetailInfoController: UIViewController {

    @IBOutlet weak var lblHumidity: UILabel!
    @IBOutlet weak var lblWindspeed: UILabel!
    @IBOutlet weak var lblVisibility: UILabel!
    @IBOutlet weak var imgViewHumid: UIImageView!
    @IBOutlet weak var imgViewWind: UIImageView!
    @IBOutlet weak var imgViewVisibl: UIImageView!
    
    var detailViewModel: DetailInfoViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblHumidity.text = detailViewModel?.strHumidity
        lblWindspeed.text = detailViewModel?.strWindspeed
        lblVisibility.text = detailViewModel?.strVisibility
        configureImages()
    }
    
    private func configureImages() {
        imgViewWind.roudedCorner()
        imgViewHumid.roudedCorner()
        imgViewVisibl.roudedCorner()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
