//
//  WeatherHomeViewModel.swift
//  Accenture_Task
//
//  Created by Nupur Sharma on 18/02/21.
//  Copyright © 2021 Nupur Sharma. All rights reserved.
//

import Foundation
import Bond
import ReactiveKit

class WeatherHomeViewModel : NSObject {
    
    private var serviceManager : ServiceManager!
    private var model: WeatherModel?
    var strCity = Observable<String>("")
    var strTemp = Observable<String>("")
    var strDesc = Observable<String>("")
    
    
    override init() {
        super.init()
        self.serviceManager =  ServiceManager()
        serviceManager.apiToGetEmployeeData { [weak self] (model) in
            if let model = model {
                self?.setupData(model: model)
            }
        }
    }
    
    private func setupData(model: WeatherModel) {
        strCity.value = model.name
        strTemp.value = model.temp
        strDesc.value = model.desc
        self.model = model
    }
    
    func getDetails() -> (humid: String, speed: String, visiblty: String) {
        return (self.model?.humidity ?? "", self.model?.windSpeed ?? "", self.model?.visibility ?? "")
    }
}
