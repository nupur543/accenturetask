//
//  DetailInfoViewModel.swift
//  Accenture_Task
//
//  Created by Nupur Sharma on 20/02/21.
//  Copyright © 2021 Nupur Sharma. All rights reserved.
//

import Foundation
import ReactiveKit
import Bond

class DetailInfoViewModel {
    
    var strHumidity = String()
    var strWindspeed = String()
    var strVisibility = String()

    init(humid: String, speed: String, visiblty: String) {
        strHumidity = humid
        strWindspeed = speed
        strVisibility = visiblty
    }
       
}
