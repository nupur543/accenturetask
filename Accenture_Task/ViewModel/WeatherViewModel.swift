//
//  WeatherViewModel.swift
//  Accenture_Task
//
//  Created by Nupur Sharma on 18/02/21.
//  Copyright © 2021 Nupur Sharma. All rights reserved.
//

import Foundation
import Bond
import ReactiveKit
import CoreData

class WeatherViewModel : NSObject {
    
private var serviceManager : ServiceManager!
let disposeBag = DisposeBag()
var arrWeatherModel = Observable<[WeatherModel]>([])

   override init() {
       super.init()
    //    getSavedData()

   }
    
     func getSavedData() {
        guard let appDel = UIApplication.shared.delegate as? AppDelegate else { return  }
        
       let viewContext = appDel.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
       
        do {
            let results = try viewContext.fetch(request) as! [Weather]
            let arrModels: [WeatherModel] = results.map{WeatherModel(temp : $0.temp ?? "",desc: $0.desc ?? "",date: $0.date ?? "")}
            arrWeatherModel.value = arrModels
            
        } catch let error as NSError {
            print("Could not save ")
        }
    }
    
    func deleteData(indexPath:Int) {
        let time = arrWeatherModel.value[indexPath].date
        guard let appDel = UIApplication.shared.delegate as? AppDelegate else { return  }
               
        let viewContext = appDel.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
        request.predicate = NSPredicate(format: "date = %@", time)
        
        do {
            let results = try viewContext.fetch(request) as! [Weather]
            viewContext.delete(results[0])
            do {
                try viewContext.save()
                arrWeatherModel.value = arrWeatherModel.value.filter({$0.date != time})
            }catch {
                print("error saving data")
            }
        } catch let error as NSError {
            print("Could not save \(error.description)")
        }
              
    }
}
