//
//  EditInfoViewModel.swift
//  Accenture_Task
//
//  Created by Nupur Sharma on 20/02/21.
//  Copyright © 2021 Nupur Sharma. All rights reserved.
//

import Foundation
import ReactiveKit
import Bond
import CoreData

class EditInfoViewModel {
    
    private var model : WeatherModel?
    var temp = String()
    var desc = String()
    var date = String()
    
    init(temp: String, desc: String, date: String) {
        self.temp = temp
        self.desc = desc
        self.date = date
    }
    
    func updateInfo(txtTemp: String, txtDesc: String) {
        
        guard let appDel = UIApplication.shared.delegate as? AppDelegate else { return  }
        let viewContext = appDel.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
        request.predicate = NSPredicate(format: "date = %@", self.date)
        
        do {
            let results = try viewContext.fetch(request) as! [Weather]
            let objWeather = results[0] as! Weather
            objWeather.temp = txtTemp
            objWeather.date = txtDesc
            try viewContext.save()
              
        } catch let error as NSError {
            print("Could not save ")
        }
    }

}
