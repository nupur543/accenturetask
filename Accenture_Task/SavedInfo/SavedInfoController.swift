//
//  SavedInfoController.swift
//  Accenture_Task
//
//  Created by Nupur Sharma on 18/02/21.
//  Copyright © 2021 Nupur Sharma. All rights reserved.
//

import UIKit

class SavedInfoController: UIViewController {

    private var weatherViewModel : WeatherViewModel!
    private var dataSource : WeatherDatasource?
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var editBtn: UIBarButtonItem!
    
    var selectedRow: Int = 0
  
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        weatherViewModel?.getSavedData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tblView.delegate = self
        weatherViewModel = WeatherViewModel()
        self.dataSource = WeatherDatasource()
        tblView.dataSource = self.dataSource
        self.dataSource?.deleteData = { [weak self] row in
            self?.weatherViewModel?.deleteData(indexPath: row)
        }
        bindViewModel()
    }

    fileprivate func bindViewModel() {
        indicator.startAnimating()
        let _ = weatherViewModel.arrWeatherModel.observeNext { [weak self] (arrModel) in
           // if  arrModel.count > 0 {
                DispatchQueue.main.async {
                    self?.dataSource?.formData = arrModel
                    self?.updateTable()
                    self?.indicator.stopAnimating()
                }
            if arrModel.count == 0 {
                self?.noDataView.isHidden = false
                self?.tblView.isHidden = true
                self?.editBtn.isEnabled = false
            }else {
                self?.noDataView.isHidden = true
                self?.tblView.isHidden = false
                self?.editBtn.isEnabled = true
            }
        }
    }
    
    private func updateTable() {
        
        tblView.reloadData()
    }
    @IBAction func editPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "editInfoSegue", sender: self)
    }
    
    @IBAction func dismissPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier  == "editInfoSegue" {
            let dest = segue.destination as! EditInfoViewController
            guard let data = dataSource?.formData[selectedRow] else { return  }
            let vm = EditInfoViewModel(temp: data.temp, desc: data.desc, date: data.date)
            dest.editInfoViewModel = vm
        }
    }
}

extension SavedInfoController: UITableViewDelegate {
    
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
     {
        if editingStyle == .delete {
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRow = indexPath.row
    }
}
