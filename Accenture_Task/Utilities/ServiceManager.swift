//
//  ServiceManager.swift
//  Accenture_Task
//
//  Created by Nupur Sharma on 18/02/21.
//  Copyright © 2021 Nupur Sharma. All rights reserved.
//

import Foundation
class ServiceManager :  NSObject {
    
    private let sourcesURL = URL(string: "https://api.openweathermap.org/data/2.5/weather?q=London,uk&APPID=1b9e6de92e46fdb46960ac388662a926")!
    
    func apiToGetEmployeeData(completion : @escaping (WeatherModel?) -> ()){
        URLSession.shared.dataTask(with: sourcesURL) { (data, urlResponse, error) in
            if let data = data {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        if let dataObj = WeatherModel(json: json) {
                            completion(dataObj)
                        }else {
                            completion(nil)
                        }
                    }else {
                        completion(nil)
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    completion(nil)
                }
            }
        }.resume()
    }
}
