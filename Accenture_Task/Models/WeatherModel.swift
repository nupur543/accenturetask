//
//  WeatherModel.swift
//  Accenture_Task
//
//  Created by Nupur Sharma on 18/02/21.
//  Copyright © 2021 Nupur Sharma. All rights reserved.
//

import Foundation

struct WeatherModel {
    var name: String = ""
    var temp: String = ""
    var desc: String = ""
    var date: String = ""
    var humidity: String = ""
    var windSpeed: String = ""
    var visibility: String = ""
    
    init(name: String = "", temp: String, desc: String, date: String) {
        self.name = name
        self.temp = temp
        self.desc = desc
        self.date = date
    }
    
    init?(json: [String: Any]) {
       // 1.8(K - 273) + 32
        guard let name = json["name"] as? String,
            let visibil = json["visibility"] as? Int,//m
            let windJSON = json["wind"] as? [String: Any],
            let mainJSON = json["main"] as? [String: Any],
            let arrWeatherJSON = json["weather"] as? [[String: Any]],
            let weatherJSON = arrWeatherJSON.first
        else {
            return nil
        }
        
        self.name = name
        self.visibility =  "\(visibil) m"
        
        if let tempK = mainJSON["temp"] as? Double ,let humid =  mainJSON["humidity"] as? Double{
            let farenTemp = 1.8 * (tempK - 273) + 32
            self.temp = String(format: "%.2f °F", farenTemp)
            self.humidity = "\(humid) %"
        }
        if let weatherDesc = weatherJSON["description"] as? String {
            self.desc = weatherDesc
        }
        if let speed = windJSON["speed"] as? Double {
            self.windSpeed = "\(speed) m/s"
        }
    }
}
